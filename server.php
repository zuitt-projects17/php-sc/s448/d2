<?php

session_start(); //creates a session link between pages that allows session data to be passed around

//create a class called TaskList that will contain all our CRUD functionalities
class TaskList{
	//method to add a new task
	public function add($description){ //accepts one argument named description

		//creates a new object named $newTask. The description passed in the method's argument will be the value of the $newTask object's description property.
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		//check if the tasks global array exists yet or not
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array(); //if it does yet exist, create an empty array
		}

		//$_SESSION[name] is a superglobal variable in PHP that allows data to be sent and received across different files

		array_push($_SESSION['tasks'], $newTask); //add the new task to our session array 
	}
	//method to update existing task
	public function update($id, $description, $isFinished){
		//update a task's description to reflect the new description
		//the $id argument denotes which task to be update, since it is used to find the task's array index
		$_SESSION['tasks'][$id]->description = $description;
		//toggle the task's isFinished property
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}
	//method to remove a single task
	public function remove($id){
		array_splice($_SESSION['tasks'], $id, 1);
	}
	//method to remove all tasks
	public function clear(){
		session_destroy(); //removes all session data
	}
};

$taskList = new TaskList(); //create a new object from our TaskList class

//check if the submitted action was to add
if($_POST['action'] === 'add'){
	//if true, receive the description being sent via POST, and pass it to the add() method to add a new task to our session array
	$taskList->add($_POST['description']);

}else if($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);

}else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);

}else if($_POST['action'] === 'clear'){
	$taskList->clear();
};

header('Location: ./index.php'); //redirects the user back to index.php while maintaining the session
