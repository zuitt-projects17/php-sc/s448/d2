<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>S5: Client-Server Communication (To-Do List)</title>
</head>
<body>
	<?php session_start(); ?>

	<h3>Add Task</h3>
	<!-- The action attribute determines where/what file your form data is sent -->
	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="add">
		<!-- send action=add as a key/value pair -->
		Description: <input type="text" name="description" required>
		<!-- send description=[whatever the user typed] as a key/value pair -->
		<button type="submit">Add</button>
	</form>

	<h3>Task List</h3>
	<?php if(isset($_SESSION['tasks'])): //check if the tasks session array exists ?>
		<?php foreach($_SESSION['tasks'] as $index => $task): //loop through our tasks to display their info ?>
			<form method="POST" action="./server.php" style="display: inline-block;">
				
				<input type="hidden" name="action" value="update">
				<!-- hidden input storing the task's array index as its id -->
				<input type="hidden" name="id" value="<?= $index; ?>">
				<!-- checkbox that will use a ternary operator to be checked if a task is finished, unchecked if not -->
				<input type="checkbox" name="isFinished" <?= $task->isFinished ? 'checked' : null; ?>>
				<!-- show an input containing the name of each task -->
				<input type="text" name="description" value="<?= $task->description; ?>">
				<input type="submit" value="Update">

			</form>

			<form method="POST" action="./server.php" style="display: inline-block;">

				<input type="hidden" name="action" value="remove">
				<input type="hidden" name="id" value="<?= $index; ?>">
				<input type="submit" value="Delete">

			</form>
		<?php endforeach; ?>
	<?php endif; ?>

	<br/><br/>

	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="clear">
		<button type="submit">Clear all tasks</button>
	</form>
</body>
</html>
